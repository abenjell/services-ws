package com.book.akacia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by Adil on 10/09/2018.
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.book.akacia")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
