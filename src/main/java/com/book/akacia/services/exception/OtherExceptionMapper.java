package com.book.akacia.services.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.springframework.stereotype.Component;

import com.book.akacia.services.bo.Erreur;

@Component("otherExceptionMapper")
@Provider
public class OtherExceptionMapper implements ExceptionMapper<Exception> {
	
	private static final String CODE = "error.unexpected";

	public Response toResponse(Exception e) {
		if (e instanceof WebApplicationException) {
			Response response = ((WebApplicationException) e).getResponse();
			String code = "error." + response.getStatus();
			return Response.status(response.getStatus()).entity(new Erreur(code,  e.getMessage())).type(MediaType.APPLICATION_JSON_TYPE).build();
		}
		
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new Erreur(CODE,  e.getMessage())).build();

	}

}
