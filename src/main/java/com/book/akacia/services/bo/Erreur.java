package com.book.akacia.services.bo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Erreur {

	private final String message;

	private final String code;

	public Erreur(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public String getCode() {
		return code;
	}
}
