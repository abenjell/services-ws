package com.book.akacia.services.ws.impl;

import javax.annotation.Resource;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Service;

import com.book.akacia.dao.bean.AddressDAO;
import com.book.akacia.services.bo.SearchAddressResponse;
import com.book.akacia.services.ws.AddressRestService;

@Service("addressRestService")
public class AddressRestServiceImpl implements AddressRestService {
	
	@Resource
    private AddressDAO addressDAO;

	public Response getAddress(String latitude, String longitude, String radius) {
		SearchAddressResponse addressList = new SearchAddressResponse(addressDAO.searchAddress(Double.valueOf(latitude), Double.valueOf(longitude), Integer.valueOf(radius)));
		return Response.ok(addressList).build();
	}
}
