package com.book.akacia.services.bo;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="markers")
public class SearchAddressResponse {
	
	public SearchAddressResponse() {
		super();
	}

	public SearchAddressResponse(List<Address> addressList) {
		super();
		this.addressList = addressList;
	}

	private List<Address> addressList;

	@XmlElement(name="marker")
	public List<Address> getAddressList() {
		return addressList;
	}

	public void setAddressList(List<Address> addressList) {
		this.addressList = addressList;
	}
	
}
