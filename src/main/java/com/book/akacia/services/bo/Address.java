package com.book.akacia.services.bo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Address {

	private String name;

	private String AddressLabel;

	private String comment;

	private String horaireVendredi;

	private Double lat;

	private Double lng;

	private Double distance;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddressLabel() {
		return AddressLabel;
	}

	public void setAddressLabel(String addressLabel) {
		AddressLabel = addressLabel;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getHoraireVendredi() {
		return horaireVendredi;
	}

	public void setHoraireVendredi(String horaireVendredi) {
		this.horaireVendredi = horaireVendredi;
	}

}
