package com.book.akacia.dao.query;

import com.book.akacia.services.bo.Address;
import org.springframework.jdbc.object.MappingSqlQuery;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AddressMappingQuery extends MappingSqlQuery<Address> {
    
    @Override
    protected Address mapRow(ResultSet rs, int rowNumber) throws SQLException {
    	Address address = new Address();
        address.setAddressLabel(rs.getString("address"));
        address.setName(rs.getString("name"));
        address.setHoraireVendredi(rs.getString("hfriyday"));
        address.setComment(rs.getString("comment"));
        address.setLat(rs.getDouble("lat"));
        address.setLng(rs.getDouble("lng"));
        address.setDistance(rs.getDouble("distance"));
        return address;
    }

}