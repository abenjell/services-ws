package com.book.akacia.spring.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Created by Adil on 01/12/2018.
 */
@Configuration
@ImportResource({"classpath:spring/sfc-services-ws-context.xml", "classpath:database/sfc-dao-context.xml"})
public class AppConfiguration {
}
