package com.book.akacia.dao.bean;

import java.util.List;

import javax.annotation.Resource;

import com.book.akacia.dao.query.AddressMappingQuery;
import com.book.akacia.services.bo.Address;

public class AddressDAO {

	@Resource
	private AddressMappingQuery addressMappingQuery;
	
	public List<Address> searchAddress(Double lat, Double lng, Integer radius){
		return addressMappingQuery.execute(lat, lng, lat, radius);
	}
}
