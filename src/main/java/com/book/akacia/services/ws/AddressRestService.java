package com.book.akacia.services.ws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/address")
public interface AddressRestService {
    @GET
    @Path("/search")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    Response getAddress(@QueryParam("lat") String latitude, @QueryParam("lng") String longitude,@QueryParam("radius") String radius);
}
